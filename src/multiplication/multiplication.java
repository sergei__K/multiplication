package multiplication;

import java.util.Scanner;

public class multiplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int negative = 0;

        if (a != 0 && b != 0) {
            if (a < 0) {
                a = -a;
                negative++;

            }
            if (b < 0) {
                b = -b;
                negative++;
            }
            if (b > a) {
                int temporary = a;
                a = b;
                b = temporary;
            }
            System.out.println(multiplication(a, b, negative));
        } else {
            System.out.println(0);
        }
    }

    /**
     * Метод для уножения переменных без знака умножения.
     *
     * @param a
     * @param b
     * @param negative
     * @return
     */
    public static int multiplication(int a, int b, int negative) {
        int i = 0;
        int result = 0;
        while (i < b) {
            result += a;
            i++;
        }
        if (negative == 1) {
            result = -result;
        }
        return result;
    }
}
